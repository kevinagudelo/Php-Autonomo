<?php

/*
  Los métodos en PHP son bastante útiles para poder encapsular una funcionalidad especifica en un solo lugar y poder reutilizar ese código muchas veces. Es por ello que vamos a ver como podemos crear un método en PHP.
 */

// clase base con propiedades y métodos miembro
class Verdura {

  var $comestible;
  var $color;

  function Verdura($comestible, $color = "verde") {
    $this->comestible = $comestible;
    $this->color = $color;
  }

  function es_comestible() {
    return $this->comestible;
  }

  function qué_color() {
    return $this->color;
  }

}

// fin de la clase Verdura
// ampliar la clase base
class Espinaca extends Verdura {

  var $concinada = false;

  function Espinaca() {
    $this->Verdura(true, "verde");
  }

  function cocinarla() {
    $this->concinada = true;
  }

  function está_cocinada() {
    return $this->concinada;
  }

}

// fin de la clase Espinaca