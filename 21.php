<?php

/*
 * $ _SERVER es una variable super global de PHP que contiene información sobre encabezados, rutas y ubicaciones de guiones.
 */
echo $_SERVER['PHP_SELF'];
echo "<br>";
echo $_SERVER['SERVER_NAME'];
echo "<br>";
echo $_SERVER['HTTP_HOST'];
echo "<br>";
echo $_SERVER['HTTP_REFERER'];
echo "<br>";
echo $_SERVER['HTTP_USER_AGENT'];
echo "<br>";
echo $_SERVER['SCRIPT_NAME'];
/*
 * PHP $ _REQUEST se utiliza para recopilar datos después de enviar un formulario HTML.
 */
