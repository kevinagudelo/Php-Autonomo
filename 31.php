<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Aunque este lenguaje de programación no es muy estricto en cuanto a POO, se pueden emular la mayoría de cosas que son necesarias en Objetos, en este caso Encapsulamiento. Para comenzar es bueno tener en cuenta la ocultación de información(encapsulamiento). Muchos dicen que PHP rompe el encapsulamiento debido a que nos permite acceder a cualquier atributo de una clase directamente, pero en si hay una forma de hacer que no se pueda y así generar el encapsulamiento. Para ello tenemos el concepto de visibilidad en php.
//El concepto de visibilidad nos dice que hay tres palabras claves/reservadas: public, protected y private que anteponiendolas a cualquier método o atributo/propiedad se podrán modificar o implementar su visibilidad, haciendo posible el encapsulamiento.
//
//Public: Se podrá acceder a ella de cualquier forma, directamente o por métodos de la misma o de otras clases. Ejemplo:

class Persona {

  public $nombre;
  public $apellido;
  public $edad; // Es public por lo que se accede desde

//      cualquier parte

  public function __construct($nom, $ape, $e) {
    $this->nombre = $nom;
    $this->apellido = $ape;
    $this->edad = $e;
  }

  public function imprimir() {
    return $this->edad;
  }

}

$persona = new Persona('Pepito', 'Perez', 24);
echo $persona->edad;
echo $persona->imprimir();

//Como vemos es la misma clase persona que se viene trabajando. Creamos el objeto y hacemos echo a la edad directamente, lo que nos imprimira un 24, despues por medio del metodo imprimir() tambien nos pondra en pantalla el mismo dato, teniendo en cuenta el return en la linea 12.
//Protected: Si cambiamos el public por el protected y dejamos lo demas igual, nos saldra un fatal error, ya que con protected no nos dejara acceder a la propiedad a menos que sea por métodos de la misma clase, herencia o con el parent.

class Persona {

  public $nombre;
  public $apellido;
  protected $edad;

  public function __construct($nom, $ape, $e) {
    $this->nombre = $nom;
    $this->apellido = $ape;
    $this->edad = $e;
  }

  public function imprimir() {
    return $this->edad;
  }

}

$persona = new Persona('Pepito', 'Perez', 24);
echo $persona->edad;
echo $persona->imprimir();

//Si comentamos la linea donde imprimimos directamente la propiedad, nos imprimira el resultado del metodo imprimir().
//echo $persona->edad;
echo $persona->imprimir();

//Private: Al poner la propiedad edad en private solo nos dejara acceder desde la misma clase, entonces si hacemos el ejercicio anterior
class Persona {

  public $nombre;
  public $apellido;
  private $edad;

  public function __construct($nom, $ape, $e) {
    $this->nombre = $nom;
    $this->apellido = $ape;
    $this->edad = $e;
  }

  public function imprimir() {
    return $this->edad;
  }

}

class trabajo extends Persona {

  public $trabajos;
  public $pruebas;

  public function trabajar($job) {
    $this->trabajos = $job;
  }

  public function imprimir() {
    return 'Mi nombre es ' . $this->nombre . ' ' . $this->
            apellido . ' y tengo ' . $this->edad . ' Años y trabajo
           en  ' . $this->trabajos;
  }

}

$persona = new Persona('Pepito', 'Perez', 24);
echo $persona->imprimir();

//Si corremos php con el siguiente código, no nos dejara acceder a la propiedad edad de la clase Persona desde trabajo porque es privada, si fuera protected si nos dejaria pero solo por metodos o parent.
//
//Esto es encapsulamiento en PHP.