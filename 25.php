<?php

/*
  Las variables pertenecientes a una clase se llaman "propiedades". También se les puede llamar usando otros términos como "atributos" o "campos", pero para los propósitos de esta referencia se va a utilizar "propiedades". Éstas se definen usando una de las palabras reservadas public, protected, o private, seguido de una declaración normal de variable. Esta declaración puede incluir una inicialización, pero esta inicialización debe ser un valor constante, es decir, debe poder ser evaluada durante la compilación y no depender de información generada durante la ejecución.
 */

class ClaseSencilla {

  // Válido a partir de PHP 5.6.0:
  public $var1 = 'hola ' . 'mundo';
  // Válido a partir de PHP 5.3.0:
  public $var2 = <<<EOD
hola mundo
EOD;
  // Válido a partir de PHP 5.6.0:
  public $var3 = 1 + 2;
  // Declaraciones de propiedades válidas:
  public $var6 = miConstante;
  public $var7 = array(true, false);
  // Válido a partir de PHP 5.3.0:
  public $var8 = <<<'EOD'
hola mundo
EOD;

}
