<?php

/*
  PHP 5 permite a los desarrolladores declarar métodos constructores para las clases. Aquellas que tengan un método constructor lo invocarán en cada nuevo objeto creado, lo que lo hace idóneo para cualquier inicialización que el objeto pueda necesitar antes de ser usado.
 */

class BaseClass {

  function __construct() {
    print "En el constructor BaseClass\n";
  }

}

class SubClass extends BaseClass {

  function __construct() {
    parent::__construct();
    print "En el constructor SubClass\n";
  }

}

class OtherSubClass extends BaseClass {
  // heredando el constructor BaseClass
}

// En el constructor BaseClass
$obj = new BaseClass();

// En el constructor BaseClass
// En el constructor SubClass
$obj = new SubClass();

// En el constructor BaseClass
$obj = new OtherSubClass();

// PHP 5 introduce un concepto de destructor similar al de otros lenguajes orientados a objetos, tal como C++. El método destructor será llamado tan pronto como no hayan otras referencias a un objeto determinado, o en cualquier otra circunstancia de finalización.
class MyDestructableClass {

  function __construct() {
    print "En el constructor\n";
    $this->name = "MyDestructableClass";
  }

  function __destruct() {
    print "Destruyendo " . $this->name . "\n";
  }

}

$obj = new MyDestructableClass();
