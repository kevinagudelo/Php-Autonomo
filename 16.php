<?php

/*
  sort () - ordena las matrices en orden ascendente
  rsort () - ordenar matrices en orden descendente
  asort () - ordena las matrices asociativas en orden ascendente, de acuerdo con el valor
  ksort () - ordenar matrices asociativas en orden ascendente, de acuerdo con la clave
  arsort () - ordenar matrices asociativas en orden descendente, de acuerdo con el valor
  krsort () - ordena las matrices asociativas en orden descendente, de acuerdo con la clave
 */
$cars = array("Volvo", "BMW", "Toyota");
sort($cars);

$clength = count($cars);
for ($x = 0; $x < $clength; $x++) {
  echo $cars[$x];
  echo "<br>";
}
//Rsort
$cars1 = array("Volvo", "BMW", "Toyota");
rsort($cars1);

$clength1 = count($cars1);
for ($x = 0; $x < $clength1; $x++) {
  echo $cars1[$x];
  echo "<br>";
}
//asort
$age = array("Peter" => "35", "Ben" => "37", "Joe" => "43");
asort($age);

foreach ($age as $x => $x_value) {
  echo "Key=" . $x . ", Value=" . $x_value;
  echo "<br>";
}

//ksort
$age1 = array("Peter" => "35", "Ben" => "37", "Joe" => "43");
krsort($age1);

foreach ($age1 as $x => $x_value) {
  echo "Key=" . $x . ", Value=" . $x_value;
  echo "<br>";
}
//arsort
$agea = array("Peter" => "35", "Ben" => "37", "Joe" => "43");
arsort($agea);

foreach ($agea as $x => $x_value) {
  echo "Key=" . $x . ", Value=" . $x_value;
  echo "<br>";
}
//krsort
$age3 = array("Peter" => "35", "Ben" => "37", "Joe" => "43");
krsort($age3);

foreach ($age3 as $x => $x_value) {
  echo "Key=" . $x . ", Value=" . $x_value;
  echo "<br>";
}