<!DOCTYPE html>
<!--
PHP $ _GET también se puede usar para recopilar datos de formulario después de enviar un formulario HTML con method = "get".

$ _GET también puede recopilar datos enviados en la URL.
-->
<html>
  <head>
    <title>TODO supply a title</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <script
<?php
echo "Study " . $_GET['subject'] . " at " . $_GET['web'];
?>
  </script>

</body>
