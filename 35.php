<?php

/*
  En pocas palabras un espacio de nombre o Namespace es un contenedor que nos permite agrupar nuestro código para darle un uso posterior de esta manera evitamos conflictos de nombre. Por ejemplo, tenemos 2 funciones con el mismo nombre esto generaría un conflicto de nombre pero mediante el uso de namespace damos solución a este problema.

  Pero la mejor definición que le puedo dar a los Namespace es que nos ayuda y sirven para organizar, restructurar mejor nuestro código.
 */

namespace Anexsoft\Helpers;

class Hello {

  public static function sayHello() {
    return 'Hello World';
  }

}
