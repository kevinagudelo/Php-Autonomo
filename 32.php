<?php

/*
Una de las posibilidades más útiles que nos ofrece el trabajar con objetos es el polimorfismo. Imagina una clase abstracta que sirve de base a cinco o seis clases “hijas“, de esta forma unes toda las funcionalidades que comparten en una misma clase y solo modificas o añades pequeños matices en las hijas. Pongamos un ejemplo, tenemos diferentes tipos de pelotas, de fútbol, baloncesto y tenis. Todas comparten su forma redonda, tu capacidad de rodar y rebotar, pero ninguna es igual a a anterior, tienen un peso diferente, una textura diferente, color, etcétera.
 */

