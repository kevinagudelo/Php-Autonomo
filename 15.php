<?php

/*
  Las matrices asociativas son muy similares a las matrices numéricas en términos de funcionalidad, pero son diferentes en términos de su índice. La matriz asociativa tendrá su índice como cadena para que pueda establecer una fuerte asociación entre la clave y los valores.
 */

/* First method to associate create array. */
$salaries = array("mohammad" => 2000, "qadir" => 1000, "zara" => 500);

echo "Salary of mohammad is " . $salaries['mohammad'] . "<br />";
echo "Salary of qadir is " . $salaries['qadir'] . "<br />";
echo "Salary of zara is " . $salaries['zara'] . "<br />";

/* Second method to create array. */
$salaries['mohammad'] = "high";
$salaries['qadir'] = "medium";
$salaries['zara'] = "low";

echo "Salary of mohammad is " . $salaries['mohammad'] . "<br />";
echo "Salary of qadir is " . $salaries['qadir'] . "<br />";
echo "Salary of zara is " . $salaries['zara'] . "<br />";
