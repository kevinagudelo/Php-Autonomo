<?php

/*
  Se pueden usar múltiples bloques catch para atrapar diferentes clases de excepciones. La ejecución normal (cuando no es lanzada ninguna excepción dentro del bloque try) continuará después del último bloque catch definido en la sencuencia. Las excepciones pueden ser lanzadas ("thrown") (o relanzadas) dentro de un bloque catch.

  Cuando una excepción es lanzada, el código siguiente a la declaración no será ejecutado, y PHP intentará encontrar el primer bloque catch coincidente. Si una excepción no es capturada, se emitirá un Error Fatal de PHP con un mensaje "Uncaught Exception ..." ("Excepción No Capturada"), a menos que se haya definido un manejador con set_exception_handler().

  finally

  En PHP 5.5 y posterior, se puede utilizar un bloque finally después o en lugar de los bloques catch. El código de dentro del bloque finally siempre se ejecutará después de los bloques try y catch, independientemente de que se haya lanzado una excepción o no, y antes de que la ejecución normal continúe.
 */

function inverse($x) {
  if (!$x) {
    throw new Exception('División por cero.');
  }
  return 1 / $x;
}

try {
  echo inverse(5) . "\n";
} catch (Exception $e) {
  echo 'Excepción capturada: ', $e->getMessage(), "\n";
} finally {
  echo "Primer finally.\n";
}

try {
  echo inverse(0) . "\n";
} catch (Exception $e) {
  echo 'Excepción capturada: ', $e->getMessage(), "\n";
} finally {
  echo "Segundo finally.\n";
}

// Continuar ejecución
echo 'Hola Mundo\n';

