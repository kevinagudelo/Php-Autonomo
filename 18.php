<?php

/*
  El constructor if es una de las características más importantes de muchos lenguajes, incluido PHP. Permite la ejecución condicional de fragmentos de código. PHP dispone de una estructura if que es similar a la de C:
 */
$a = 10;
$b = 5;
if ($a > $b) {
  echo "a es mayor que b";
}

/* Con frecuencia se desea ejecutar una sentencia si una determinada condición se cumple y una sentencia diferente si la
 *  condición no se cumple. Esto es para lo que sirve else. El else extiende una sentencia if para ejecutar una sentencia en
 * caso que la expresión en la sentencia if se evalúe como FALSE
 */
if ($a > $b) {
  echo "a es mayor que b";
} else {
  echo "a NO es mayor que b";
}
/* elseif, como su nombre lo sugiere, es una combinación de if y else. Del mismo modo que else, extiende una sentencia if para ejecutar una sentencia diferente en caso que la expresión if original se evalúe como FALSE. Sin embargo, a diferencia de else, esa expresión alternativa sólo se ejecutará si la expresión condicional del elseif se evalúa como TRUE.
 */
if ($a > $b) {
  echo "a es mayor que b";
} elseif ($a == $b) {
  echo "a es igual que b";
} else {
  echo "a es menor que b";
}

/* El significado de una sentencia while es simple. Le dice a PHP que ejecute las sentencias anidadas, tanto como la expresión while se evalúe como TRUE. El valor de la expresión es verificado cada vez al inicio del bucle, por lo que incluso si este valor cambia durante la ejecución de las sentencias anidadas, la ejecución no se detendrá hasta el final de la iteración (cada vez que PHP ejecuta las sentencias contenidas en el bucle es una iteración). A veces, si la expresión while se evalúa como FALSE desde el principio, las sentencias anidadas no se ejecutarán ni siquiera una vez. */
$i = 1;
while ($i <= 10):
  echo $i;
  $i++;
endwhile;

/* Los bucles do-while son muy similares a los bucles while, excepto que la expresión verdadera es verificada al final de cada
 * iteración en lugar que al principio. La diferencia principal con los bucles while es que está garantizado que corra la
 *  primera iteración de un bucle do-while (la expresión verdadera sólo es verificada al final de la iteración), mientras que
 *  no necesariamente va a correr con un bucle while regular (la expresión verdadera es verificada al principio de cada
 *  iteración, si se evalúa como FALSE justo desde el comienzo, la ejecución del bucle terminaría inmediatamente).

 */
$e = 0;
do {
  echo $e;
  $e++;
} while ($e < 10);

/*
 * Los bucles for son los más complejos en PHP. Se comportan como sus homólogos en C. La sintaxis de un bucle for es:

  for (expr1; expr2; expr3)
  sentencia
  La primera expresión (expr1) es evaluada (ejecutada) una vez incondicionalmente al comienzo del bucle.

  En el comienzo de cada iteración, se evalúa expr2. Si se evalúa como TRUE, el bucle continúa y se ejecutan la/sy sentencia/s
 * anidada/s. Si se evalúa como FALSE, finaliza la ejecución del bucle.

  Al final de cada iteración, se evalúa (ejecuta) expr3.

  Cada una de las expresiones puede estar vacía o contener múltiples expresiones separadas por comas. En expr2, todas las
 *  expresiones separadas por una coma son evaluadas, pero el resultado se toma de la última parte. Que expr2 esté vacía
 *  significa que el bucle debería ser corrido indefinidamente (PHP implícitamente lo considera como TRUE, como en C). Esto
 *  puede no ser tan inútil como se pudiera pensar, ya que muchas veces se debe terminar el bucle usando una sentencia
 *  condicional break en lugar de utilizar la expresión verdadera del for.
 */
for ($i = 1; $i <= 10; $i++) {
  echo $i;
}
/*
 * El constructor foreach proporciona un modo sencillo de iterar sobre arrays. foreach funciona sólo sobre arrays y objetos, y
 *  emitirá un error al intentar usarlo con una variable de un tipo diferente de datos o una variable no inicializada
 */
$array = array(1, 2, 3, 4);
foreach ($array as &$valor) {
  $valor = $valor * 2;
}echo $array;
/*
  break finaliza la ejecución de la estructura for, foreach, while, do-while o switch en curso.
 */
$arr = array('uno', 'dos', 'tres', 'cuatro', 'pare', 'cinco');
while (list(, $val) = each($arr)) {
  if ($val == 'pare') {
    break;    /* Se puede también escribir 'break 1;' aquí. */
  }
  echo "$val<br />\n";
}
/*
 * continue se utiliza dentro de las estructuras iterativas para saltar el resto de la iteración actual del bucle y continuar
 *  la ejecución en la evaluación de la condición, para luego comenzar la siguiente iteración.
 */
while (list($clave, $valor) = each($arr)) {
  if (!($clave % 2)) { // saltar los miembros pares
    continue;
  }
  hacer_algo($valor);

  while ($i++ < 5) {
    echo "Exterior<br />\n";
    while (1) {
      echo "Medio<br />\n";
      while (1) {
        echo "Interior<br />\n";
        continue 3;
      }
      echo "Esto nunca se imprimirá.<br />\n";
    }
    echo "Ni esto tampoco.<br />\n";
  }
  /*
    La sentencia switch es similar a una serie de sentencias IF en la misma expresión. En muchas ocasiones, es posible que se quiera comparar la misma variable (o expresión) con muchos valores diferentes, y ejecutar una parte de código distinta dependiendo de a que valor es igual. Para esto es exactamente la expresión switch.
   */
  if ($i == 0) {
    echo "i es igual a 0";
  } elseif ($i == 1) {
    echo "i es igual a 1";
  } elseif ($i == 2) {
    echo "i es igual a 2";
  }

  switch ($i) {
    case 0:
      echo "i es igual a 0";
      break;
    case 1:
      echo "i es igual a 1";
      break;
    case 2:
      echo "i es igual a 2";
      break;
  }
  /* return devuelve el control del programa al módulo que lo invoca. La ejecución vuelve a la siguiente expresión después del
    | *  módulo que lo invoca.
   */

  include("b.php");
  echo "a";



  echo "b";
  return;

  include("b.php");
  echo "a";


  echo "b";
  exit;

//require es idéntico a include excepto que en caso de fallo producirá un error fatal de nivel E_COMPILE_ERROR. En otras palabras, éste detiene el script mientras que include sólo emitirá una advertencia (E_WARNING) lo cual permite continuar el script.*/

  require 'somefile.php';
//La sentencia include incluye y evalúa el archivo especificado.
//vars.php
//<?php

  $color = 'verde';
  $fruta = 'manzana';

//
//test.php
//<?php

  echo "Una $fruta $color"; // Una

  include 'vars.php';

  echo "Una $fruta $color"; // Una manzana verde
//*La sentencia require_once es idéntica a require excepto que PHP verificará si el archivo ya ha sido incluido y si es así, no se incluye (require) de nuevo.*/
  /*
   * La sentencia include_once incluye y evalúa el fichero especificado durante la ejecución del script. Tiene un comportamiento similar al de la sentencia include, siendo la única diferencia de que si el código del fichero ya ha sido incluido, no se volverá a incluir, e include_once devolverá TRUE. Como su nombre indica, el fichero será incluido solamente una vez.
   */
