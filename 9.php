<?php

//La función addcslashes () devuelve una cadena con barras invertidas delante de los caracteres especificados.
$str = addcslashes("Hello World!", "W");
echo($str);
//La función addslashes () devuelve una cadena con barras invertidas delante de caracteres predefinidos.
$str1 = addslashes('What does "yolo" mean?');
echo($str1);
//La función bin2hex () convierte una cadena de caracteres ASCII en valores hexadecimales.
$str2 = bin2hex("Hello World!");
echo($str2);
//La función chop () elimina espacios en blanco u otros caracteres predefinidos del extremo derecho de una cadena.
$str3 = "Hello World!";
echo $str3 . "<br>";
echo chop($str3, "World!");
