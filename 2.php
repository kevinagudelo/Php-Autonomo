<?php

//Booleanos:
$guapo = true;
$simpatico = false;
//Enteros
$cero = 0;
$ocho = 8;
$negativo = -3;
//Decimales
$mi_nota = 7.5;
$tu_nota = 8.67;
$mi_negativo = -2.32;
//Cadenas
$mi_cadena = "hola, mundo.";
