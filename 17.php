<?php

$a = 3;
$b = 2;
+$a; //Identidad	Conversión de $a a int o float según el caso.
-$a; //Negación	Opuesto de $a.
echo ($a + $b);  //Adición	Suma de $a y $b.
echo ($a - $b); //Sustracción	Diferencia de $a y $b.
echo ($a * $b); //	Multiplicación	Producto de $a y $b.
echo ($a / $b); //División	Cociente de $a y $b.
echo ($a % $b); //Módulo	Resto de $a dividido por $b.
echo ($a ** $b); //Exponenciación	Resultado de elevar $a a la potencia $bésima. Introducido en PHP 5.6.